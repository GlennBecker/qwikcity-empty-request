import { EndpointHandler } from "@builder.io/qwik-city";

export const post: EndpointHandler = async (ev) => {
  try {
    let data = await ev.request.formData();
    console.log(data);
  }
  catch (e) {
    console.log('Form Data Error', e)
  }
  console.log('Headers');
  ev.request.headers.forEach((value, key) => console.log('  -', key, value));

  console.log('URL Search', ev.url.search);
  return new Response('', {
    status: 301,
    headers: {
      location: '/',
      'Set-Cookie': 'hello-qwikcity=true'
    }
  })
}