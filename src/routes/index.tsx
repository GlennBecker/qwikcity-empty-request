import { component$, Host } from '@builder.io/qwik';

export default component$(() => {
  return (
    <Host>
      <h1>Request Data Is Always Empty</h1>

      <p>Use the forms below to post a form submission to the api/on-submission endpoint</p>
      <p>The url search, form data and headers are always empty.</p>

      <h2>Default url encoding</h2>
      <form action="/api/on-submission" method="post">
        <input type="text" readOnly name="greeting" value="Hello Qwik!" />
        <button>Submit</button>
      </form>

      <h2>Multipart form data</h2>
      <form action="/api/on-submission" method="post" encType='multipart/form-data'>
        <input type="text" readOnly name="greeting" value="Hello Qwik!" />
        <button>Submit</button>
      </form>
    </Host>
  );
});
